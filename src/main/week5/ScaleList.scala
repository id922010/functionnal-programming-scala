package week5

object ScaleList extends App {
  def scaleList(xs: List[Double], f: Double): List[Double] = {
    xs.map { x => x * f }
  }
  
  def squareList(xs: List[Int]): List[Int] =
    xs match {
      case Nil => xs
      case y :: ys => y * y :: squareList(ys)
    }
  
  def squareList2(xs: List[Int]): List[Int] =
    xs.map { x => x * x }

  def posElem(xs: List[Int]): List[Int] = 
    xs filter (x => x > 0)

  def pack[T](xs: List[T]): List[List[T]] = xs match {
    case Nil => Nil
    case x :: xs1 => 
      val (fl, ll) = xs span (y => y == x)
      fl :: pack(ll)
  }
  
  def encode[T](xs: List[T]): List[(T, Int)] = {
    pack(xs).map { x => (x.head, x.size) }
  }
  
  val aList = List(1.0, 2.0, 3.0, 4.0)
  val bList = List(1, 2, 3, 4)
  val cList = List(1, -2, 3, -4)
  val dList = List("a", "a", "a", "b", "c", "c", "a")
  println("scaleList: " + scaleList(aList, 2.0))
  println("squareList: " + squareList(bList))
  println("squareList2: " + squareList2(bList))
  println("posElem: " + posElem(cList))
  println("pack: " + pack(dList))
  println("encode: " + encode(dList))
}
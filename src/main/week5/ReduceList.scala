package week5

object ReduceList extends App {
  
  // Each _ represents an implicit paramater coming from the functional call
  def sum(xs: List[Int]) = (0 :: xs) reduceLeft (_ + _)
  def prod(xs: List[Int]) = (1 :: xs) reduceLeft(_ * _)
  
  def mapFun[T, U](xs: List[T], f: T => U): List[U] =
    (xs foldRight List[U]())( (x, y) => f(x) :: y  )

  def lengthFun[T](xs: List[T]): Int =
    (xs foldRight 0)( (x, y) => y + 1 )

  val aList = List(1.0, 2.0, 3.0, 4.0)
  val bList = List(1, 2, 3, 4)
  val cList = List(1, -2, 3, -4)
  val dList = List("a", "a", "a", "b", "c", "c", "a")

  println("sum: " + sum(bList))
  println("prod: " + prod(bList))
  println("scaleList: " + bList.reduceLeft((x, y) => x + y))
  println("foldRight: " + bList.foldRight(0)((x,y) => x + y))
  println("lengthFun: " + lengthFun(dList))
  println("mapFun: " + mapFun[String, String](dList, x => x.toUpperCase()))

}
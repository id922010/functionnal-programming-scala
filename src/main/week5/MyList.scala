package week5

import scala.math.Ordering

object MyList extends App {
  
  def last[T](xs: List[T]): T = xs match {
    case List() => throw new NoSuchElementException("List is empty")
    case List(x) => x
    case y :: ys => last(ys)
  }
  
  def init[T](xs: List[T]): List[T] = xs match {
    case List() => throw new NoSuchElementException("List is empty")
    case List(x) => List()
    case y :: ys => y :: init(ys)
  }
  
  def concat[T](xs: List[T], ys: List[T]): List[T] = xs match {
    case List() => ys
    case z :: zs => z :: concat(zs, ys)
  }
  
  def reverse[T](xs: List[T]): List[T] = xs match {
    case List(x) => List(x)
    case y :: ys => reverse(ys) ::: List(y)
  }
  
  def removeAt[T](xs: List[T], n: Int): List[T] = xs match {
    case List() => throw new IndexOutOfBoundsException("Out of bounds")
    case y :: ys => if (n == 0) ys else y :: removeAt(ys, n - 1)
  }
  
  def mergeSort[T](xs: List[T])(implicit ord: Ordering[T]): List[T] = {
    val n = xs.length / 2
    if (n == 0) xs
    else {
      def merge(xs: List[T], ys: List[T]): List[T] = (xs, ys) match {
        case (Nil, ys) => ys
        case (xs, Nil) => xs
        case (x :: xs1, y :: ys1) => if (ord.lt(x, y)) x :: merge(xs1, ys) else y :: merge(xs, ys1)
      }
      val xl = xs.take(n)
      val xr = xs.drop(n)
      merge(mergeSort(xl), mergeSort(xr))
    }
  }

  val aList = List('a', 'b', 'e', 'g')
  val bList = List('b', 'c', 'f', 'i')
  val cList = List(10, 5, 26, 9, 1, 7, 12)
  val dList = List("Orange", "Apple", "Banana", "Pineaple")
  println("last: " + last(aList))
  println("init: " + init(aList))
  println("concat: " + concat(aList, aList))
  println("reverse: " + reverse(aList))
  println("reverseAt: " + removeAt(aList, 2))
  println("mergedListC:" + mergeSort(cList))
  println("mergedListD:" + mergeSort(dList))
}

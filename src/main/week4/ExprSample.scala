package week4

object ExprSample extends App {
  val n1 = Number(10)
  val n2 = Number(3)
  
  println("n1 = " + n1.show)
  println("n2 = " + n2.show)
  println("Sum(n1, n2) = " + Sum(n1, n2).show)
}
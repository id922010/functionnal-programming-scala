package week2

import math.abs

object fixedPoint extends App{
  val tolerance = 0.0001
  def iscloseEnough(x: Double, y: Double): Boolean = {
    abs((x - y) / x) / x < tolerance
  }
  
  def fixedPoint(f: Double => Double)(firstGuess: Double): Double = {
    def iterate(guess: Double): Double = {
      val next = f(guess)
      if (iscloseEnough(guess, next)) guess
      else iterate(next)
    }
    iterate(firstGuess)
  }
  
  def averageDamp(f: Double => Double)(x: Double) = (x + f(x)) / 2.0
  
  println(fixedPoint(x => 1 + x / 2)(1.0))
  
  def sqrt(x: Double): Double = fixedPoint(y => (y + x / y) / 2.0)(1.0)
  def sqrtDamp(x: Double): Double = fixedPoint(averageDamp(y => x / y))(1.0)
  
  println(sqrt(4.0))
  println(sqrtDamp(4.0))
}
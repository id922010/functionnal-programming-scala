package week2

import scala.math.abs

object rationals extends App{
  val x = new Rational(1,3)
  val y = new Rational(5,7)
  val z = new Rational(3,2)
  
  // val invalid =  new Rational(1, 0)

  println("1/3 + 1/3 = " + x.add(x))
  println("neg(5/7) = " + y.neg)
  println("1/3 - 5/7 - 3/2 = " + x.sub(y).sub(z))
}

class Rational(x: Int, y: Int) {
  require(y != 0, "denominator must not be equal to zero")
  
  private def gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)
  private val g = abs(gcd(x, y))

  def numer = x / g
  def denom = y / g
  
  def add(other: Rational): Rational = 
    new Rational(
        numer * other.denom + other.numer * denom, denom * other.denom
        )
  
  def sub(other: Rational): Rational = add(other.neg)
  
  def neg: Rational = new Rational(-numer, denom)
  
  def less(other: Rational) = numer * other.denom < other.numer * denom
  
  def max(other: Rational) = if (this.less(other)) other else this
  
  def equals(other: Rational) = numer == other.numer && denom == other.denom
  
  override def toString(): String = 
    numer + "/" + denom
}
package week2

object highOrderFunction {
  def recSum(f: Int => Int, a: Int, b: Int): Int = {
    if (a > b) 0 else f(a) + recSum(f, a + 1, b)
  }

  def sum(f: Int => Int, a: Int, b: Int): Int = {
    def loop(a: Int, acc: Int): Int = {
      if (a > b) acc
      else loop(a + 1, acc + f(a))
    }
    loop(a, 0)
  }
  
  def currySum(f: Int => Int)(a: Int, b: Int): Int = {
    if (a > b) 0 else f(a) + currySum(f)(a + 1, b)
  }
  
  def product(f: Int => Int)(a: Int, b: Int): Int = {
    if (a > b) 1 else f(a) * product(f)(a + 1, b)
  }
  
  def mapReduce(f: Int => Int, combine: (Int, Int) => Int, neutral: Int)(a: Int, b: Int): Int = {
    if (a > b) neutral else combine(f(a), mapReduce(f, combine, neutral)(a + 1, b))
  }
}
package week3

object ListSample extends App {
  val l1 = new Cons(5, new Cons(3, new Cons(7, new Cons(14, new Nil))))
  
  def nth[T](n: Int, l: List[T]): T = {
    if (l.isEmpty || n < 0) throw new IndexOutOfBoundsException()
    else if (n == 0) l.head
    else nth(n-1, l.tail)
  }
  
  println("l1 sample: " + l1.head, l1.tail.head)
  println("l1 3rd value is: " + nth(2, l1))
  
}
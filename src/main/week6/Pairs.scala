package week6

object Pairs extends App {
  def isPrimeMap(n: Int): Boolean = {
    val values = 1 until n
    values.map(x => x == 1 || n % x != 0).reduce((a,b) => a & b)
  }

  def isPrime(n: Int): Boolean = {
    val values = 2 until n
    values.forall { x => n % x != 0 }
  }

  val n = 7
  val pairs = (1 until n) flatMap (i => 
    (1 until i) map (j => (i, j)))
  
  val filteredPairs = pairs.filter(pair => isPrime(pair._1 + pair._2))
  println("Pairs with Prime sum: " + filteredPairs)
}
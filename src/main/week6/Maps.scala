package week6

object Maps extends App{
  val captials = Map("US" -> "Wahsington", "BE" -> "Brussels")
  val fruits = List("Apple", "Pear", "Orange", "Pineaple", "Ananas")
  
  def getCapital(capital: String) = captials.get(capital) match {
    case Some(capital) => capital
    case None => "Capital is missing"
  }
  
  println("Map as Iterable: " + captials.get("US")) // Return Object Some
  println("Getting some capital: " + getCapital("US"))
  println("Map as Function: " + captials("BE"))
  
  println("Alpha sort: " + fruits.sorted)
  println("Length sort: " + fruits.sortWith(_.length < _.length))
  println("Group by first letter: " + fruits.groupBy(_.head))
}
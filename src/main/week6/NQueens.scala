package week6

object NQueens extends App{
  def queens(n: Int): Set[List[Int]] = {
    def placeQueen(k: Int): Set[List[Int]] = 
      if (k == 0) Set(List())
      else for {
        queens <- placeQueen(k - 1)
        col <- 0 until n
        if isSafe(col, queens)
      } yield col :: queens
    placeQueen(n) 
  }
    
  def isSafe(col: Int, queens: List[Int]): Boolean = {
    val row = queens.length
    val queensPair = (row-1 to 0 by -1) zip queens
    queensPair.forall { 
      case (r, c) => col != c && math.abs(col - c) != row - r}
  }
  
  def showQueens(queens: List[Int]): String = {
    val lines = 
      for (col <- queens.reverse)
        yield Vector.fill(queens.length)("* ").updated(col, "x " ).mkString
    
    "\n" + (lines mkString "\n")
  }
    
 
  println("nQueens of 4: " + (queens(4).map { x => showQueens(x) }).mkString("\n"))
}
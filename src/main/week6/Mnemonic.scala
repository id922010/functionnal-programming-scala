package week6

object Mnemonic extends App {
  val mnem = Map(
      '2' -> "ABC", '3' -> "DEF", '4' -> "GHI", '5' -> "JKL")
  
  val charCode: Map[Char, Char] =  
    for ((key, value) <- mnem; ltr <- value) yield ltr -> key
  
  println(charCode)
}
package week6

object ScalarProduct extends App {
  def scalarProduct(xs: List[Int], ys: List[Int]): Int = 
    (for ((x,y) <- xs zip ys) yield x*y).sum
    
  val list1 = List(1,4,2)
  val list2 = List(2,3,8)
  println("scalarProduct: " + scalarProduct(list1, list2))
}
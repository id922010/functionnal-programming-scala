package week1

object pascal extends App {
  def pascal(col: Int, row: Int): Int = 
    if (row == 0) 1
    else if (col == 0 || col == row) 1
    else if (col > row) throw new IllegalArgumentException("column can not be greater than row")
    else pascal(col-1, row-1) + pascal(col, row-1) 
  
    println(pascal(0,0))
    println(pascal(0,2))
    println(pascal(1,2))
    println(pascal(1,3))
    println(pascal(2,4))
    println(pascal(5,4))
}
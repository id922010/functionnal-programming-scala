package week1

import scala.math.abs

object sqrt extends App{
  def sqrt(x: Double) = {
    def sqrIter(guess: Double): Double = {
      if (isGoodEnough(guess)) guess
      else sqrIter(improveGuess(guess))
    }
    
    def isGoodEnough(guess: Double): Boolean = 
      abs(guess * guess - x) < x * 0.00001
    
    def improveGuess(guess: Double): Double = 
      (guess + x / guess) / 2
    
    sqrIter(1.0)
  }

  println(sqrt(4.0))
  println(sqrt(2.0))
  println(sqrt(0.001))
  println(sqrt(0.1e-20))
  println(sqrt(1.0e20))
}
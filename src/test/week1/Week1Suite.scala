package week1

import org.scalatest.FunSuite
import scala.math.abs

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

 @RunWith(classOf[JUnitRunner])
  class Week1Suite extends FunSuite {
  
  test("factorial 0 is 1")(assert(factorial.factorial(0) === 1))
  test("factorial 4 is 24")(assert(factorial.factorial(4) === 24))

  test("square root of 4 is about 2")(assert(abs(sqrt.sqrt(4.0) - 2.0) < 0.001))
  test("square root of 2 is about 1.414")(assert(abs(sqrt.sqrt(2.0) - 1.414) < 0.001))
  test("square root of 0.001 is about 0.0316")(assert(abs(sqrt.sqrt(0.001) - 0.0316) < 0.001))
  test("square root of 0.1e-20 is about 3.16e-11")(assert(abs(sqrt.sqrt(0.1e-20) - 3.16e-11) < 0.001))
  test("square root of 1.0e20 is about 1e10")(assert(abs(sqrt.sqrt(1.0e20) - 1e10) < 1.0e20 / 1e10))

}

package week2

import org.scalatest.FunSuite
import scala.math.abs
import scala.math.pow

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

 @RunWith(classOf[JUnitRunner])
  class Week2Suite extends FunSuite {
  
  test("RecSum of 0 to 3 is 6")(assert(highOrderFunction.recSum(x => x, 0, 3) === 6))
  test("RecSum of pow(0,2) to pow(3,2) is 14")(assert(highOrderFunction.recSum(x => x * x, 0, 3) === 14))
  
  test("Sum of 0 to 3 is 6")(assert(highOrderFunction.sum(x => x, 0, 3) === 6))
  test("Sum of pow(0,2) to pow(3,2) is 14")(assert(highOrderFunction.sum(x => x * x, 0, 3) === 14))

  test("pow(2) of product of 3 to 4 is 144")(assert(highOrderFunction.product(x => x * x)(3, 4) === 144))
  test("Fact(5) is 120")(assert(highOrderFunction.product(x => x)(1, 5) === 120))

  test("mapReduce of sum 0 to 4 is 10")(assert(highOrderFunction.mapReduce(x => x, (x,y) => x + y, 0)(0, 4) === 10))
  test("mapReduce of product 1 to 4 is 24")(assert(highOrderFunction.mapReduce(x => x, (x,y) => x * y, 1)(1, 4) === 24))
  
  val r1 = new Rational(1,4)
  val r2 = new Rational(1,2)
  test("1/4 + 1/4 = 1/2")(assert(r1.add(r1).equals(r2)))
  test("1/4 < 1/2 is true")(assert(r1.less(r2)))
  test("max(1/4, 1/2) is 1/2")(assert(r1.max(r2).equals(r2)))

}
